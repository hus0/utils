-- This script directly imports Mediation's configuration files (i.e. OVUC list, ATSL, MSL) to a database.
-- It should NOT be used normally to import such files since Mediation Web is supposed to do so.

-- If you want to use it you must first move/copy Mediation's configuration files to the /tmp directory on a database server.

-- Usage: 
--       sqlplus / as sysdba @import-md-config-from-file.sql FILENAME PARAM_NAME
-- Examples:
--       sqlplus / as sysdba @import-md-config-from-file.sql OVUC_LIST_650_v04b.XML translatorMain.tollSectionList
--       sqlplus / as sysdba @import-md-config-from-file.sql ATSL_650_v01_w.xml translatorMain.advancedTollSectionList
--       sqlplus / as sysdba @import-md-config-from-file.sql MSL_TCS3G_650.csv translatorMain.modificationSectionList.ETBOCHARGEPRX1
--       sqlplus / as sysdba @import-md-config-from-file.sql MSL_ETBO_650.csv translatorMain.modificationSectionList.ETBOCHARGE

set serveroutput on

WHENEVER SQLERROR EXIT SQL.SQLCODE

CREATE OR REPLACE DIRECTORY TMP_DIR AS '/tmp/';

DECLARE
  l_bfile  BFILE;
  l_clob   CLOB;
  l_dest_offset   INTEGER := 1;
  l_src_offset    INTEGER := 1;
  l_bfile_csid    NUMBER  := 0;
  l_lang_context  INTEGER := 0;
  l_warning       INTEGER := 0;

  filename        VARCHAR2(100):= '&1';
  param_name      VARCHAR2(100):= '&2';
BEGIN
  INSERT INTO MD.MDM_PARAMETER_CLOB (PARAM_NAME,MODULE,PARAM_TYPE,VALUE,DATE_FROM,DATE_TO,RUNTIME,VALID,FILE_NAME,DESCRIPTION)
  VALUES (
    param_name,
    'MDP',
    1,
    empty_clob(),
    sysdate,
    TIMESTAMP '2099-12-31 22:59:00',
    1,
    1,
    filename,
    'Direct import by PL/SQL due to MDS-166')
  RETURN VALUE INTO l_clob;

  l_bfile := BFILENAME ('TMP_DIR', filename);
  DBMS_LOB.fileopen(l_bfile, DBMS_LOB.file_readonly);

  DBMS_LOB.loadclobfromfile (
    dest_lob      => l_clob,
    src_bfile     => l_bfile,
    amount        => DBMS_LOB.lobmaxsize,
    dest_offset   => l_dest_offset,
    src_offset    => l_src_offset,
    bfile_csid    => l_bfile_csid ,
    lang_context  => l_lang_context,
    warning       => l_warning);
  DBMS_LOB.fileclose(l_bfile);

  COMMIT;
END;
/

EXIT
/
